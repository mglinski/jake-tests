<?php namespace Jake\Tests\Strings;

use Jake\Src\Strings\Replace;
use PHPUnit\Framework\TestCase;

class ReplaceTests extends TestCase
{
    public function testReturnIsTrue (): void
    {
        $replace = new Replace();
        $this->assertTrue($replace->returnTrue());
    }

    public function testReturnIsNotFalse (): void
    {
        $replace = new Replace();
        $this->assertNotFalse($replace->returnTrue());
    }

    public function testReplaceInStringActuallyWorks (): void
    {
        $replace = new Replace();

        $needle = 'World';
        $replacement = 'Universe';
        $haystack = 'Hello, World!';
        $replacedString = $replace->inString($needle, $replacement, $haystack);

        $this->assertEquals('Hello, Universe!', $replacedString);
    }

    public function testCaseInsensitiveReplaceWorks (): void
    {
        $replace = new Replace();

        $needle = 'world';
        $replacement = 'Universe';
        $haystack = 'Hello, World!';
        $replacedString = $replace->inString($needle, $replacement, $haystack);

        $this->assertEquals('Hello, Universe!', $replacedString);
    }

    public function testReplaceInStringReturnsFalseOnNoMatch (): void
    {
        $replace = new Replace();

        $needle = 'Momma';
        $replacement = 'Universe';
        $haystack = 'Hello, World!';
        $replacedString = $replace->inString($needle, $replacement, $haystack);

        $this->assertEquals(false, $replacedString);
    }

    public function testReplaceAtBeginningOfStringWorks (): void
    {
        $replace = new Replace();

        $needle = 'Jake';
        $replacement = 'Matt';
        $haystack = 'Jake is awesome,';
        $replacedString = $replace->atBeginningOfString($needle, $replacement, $haystack);

        $this->assertEquals('Matt is awesome,', $replacedString);
    }

    public function testReplaceAtBeginningOfStringDoesNotMatchMiddleOfString (): void
    {
        $replace = new Replace();

        $needle = 'Jake';
        $replacement = 'Matt';
        $haystack = 'Jake is awesome, Matt is not, Jake Sucks. matt is not';
        $replacedString = $replace->atBeginningOfString($needle, $replacement, $haystack);


        $this->assertEquals('Matt is awesome, Matt is not, Jake Sucks. matt is not', $replacedString);
    }

    public function testReplaceAtBeginningOfStringDoesNotMatchInvalidString (): void
    {
        $replace = new Replace();

        $needle = 'Jake';
        $replacement = 'Matt';
        $haystack = 'this string is invalid';
        $replacedString = $replace->atBeginningOfString($needle, $replacement, $haystack);


        $this->assertEquals(false, $replacedString);


    }
}
