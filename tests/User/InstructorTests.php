<?php namespace Jake\Tests\User;

use Jake\Src\User\Instructor;
use PHPUnit\Framework\TestCase;

class InstructorTests extends TestCase
{
    /**
     * @var Instructor
     */
    protected $_instructorClass;

    /**
     * @throws \Exception
     */
    public function setUp (): void
    {
        parent::setUp();
        $this->_instructorClass = new Instructor();
        $this->_instructorClass->setEmail('jakecolburn.sythe@gmail.com');
        $this->_instructorClass->setUsername('Jake');
    }

    public function testGetFormattedName (): void
    {
        $this->assertEquals('Username: Jake Email: <jakecolburn.sythe@gmail.com>', $this->_instructorClass->getFormattedName());
    }
}