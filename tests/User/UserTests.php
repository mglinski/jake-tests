<?php namespace Jake\Tests\User;

use Jake\Src\User\User;
use PHPUnit\Framework\TestCase;

class UserTests extends TestCase
{
    /**
     * @var User
     */
    protected $_userClass;

    /**
     * @throws \Exception
     */
    public function setUp (): void
    {
        parent::setUp();
        $this->_userClass = new User();
        $this->_userClass->setUsername('jake');
        $this->_userClass->setEmail('jakecolburn.sythe@gmail.com');
    }

    public function testUsername (): void
    {
        $this->assertEquals('jake', $this->_userClass->getUsername());
    }

    public function testEmail (): void
    {
        $this->assertEquals('jakecolburn.sythe@gmail.com', $this->_userClass->getEmail());
    }

    /**
     * @throws \RuntimeException
     */
    public function testInvalidEmail (): void
    {
        $this->expectException(\RuntimeException::class);
        $this->_userClass->setEmail('Invalid');
    }

    public function testGetFormattedName (): void
    {
        $this->assertEquals('jake <jakecolburn.sythe@gmail.com>', $this->_userClass->getFormattedName());
    }
}