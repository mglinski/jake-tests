<?php namespace Jake\Src\Strings;

class Replace
{
    public function returnTrue (): bool
    {
        return true;
    }

    /**
     * Find a string inside of another string and replace it with a given replacement string
     *      Return false if no match could be found
     *
     * @param string $needle      The substring to find
     * @param string $replacement The string we replace the $needle with
     * @param string $haystack    The larger string to search through for $needle
     *
     * @return string|false
     */
    public function inString (string $needle, string $replacement, string $haystack)
    {
        return stripos($haystack, $needle) === false ? false : str_ireplace($needle, $replacement, $haystack);
    }

    /**
     * Find a string at the beginning of another string only and replace it with a given replacement string
     *      Return false if no match could be found at the beginning of the string
     *
     * @param string $needle      The substring to find at the beginning of a string
     * @param string $replacement The string we replace the $needle with
     * @param string $haystack    The larger string to search through for $needle
     *
     * @return string|false
     */
    public function atBeginningOfString ($needle, $replacement, $haystack)
    {
        if (stripos($haystack, $needle) !== false && stristr($haystack, $needle) === $haystack) {
            return $replacement . substr($haystack, strlen($needle));
        }
        return false;
    }

}
