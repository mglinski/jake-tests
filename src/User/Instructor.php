<?php

namespace Jake\Src\User;


class Instructor extends User
{
    public function getFormattedName (): string
    {
        return 'Username: ' . $this->getUsername() . ' Email: <' . $this->getEmail() . '>';
    }
}

//