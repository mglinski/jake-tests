<?php namespace Jake\Src\User;

class User
{
    /**
     * @var string
     */
    protected $_username;

    /**
     * @var string
     */
    protected $_email;

    /**
     * User constructor.
     *
     * @param string $username
     * @param string $email
     *
     * @throws \Exception
     */
    public function __construct (string $username = '', string $email = '')
    {
        if (!empty($username)) {
            $this->setUsername($username);
        }

        if (!empty($email)) {
            $this->setEmail($email);
        }
    }

    public function getFormattedName (): string
    {
        return $this->getUsername() . ' <' . $this->getEmail() . '>';
    }

    /**
     * @return string
     */
    public function getUsername (): string
    {
        return $this->_username;
    }

    /**
     * @param string $username
     */
    public function setUsername (string $username): void
    {
        $this->_username = $username;
    }

    /**
     * @return string
     */
    public function getEmail (): string
    {
        return $this->_email;
    }

    /**
     * @param string $email
     *
     * @throws \RuntimeException
     */
    public function setEmail (string $email): void
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            throw new \RuntimeException('Input is not a valid email address');
        }
        $this->_email = $email;
    }
}